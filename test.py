import requests
import pandas as pd
import numpy as np
import json
import ast
import re

def call_result(url):
    URL = "http://demo.nunet.io:7006/get_score"
    
    PARAMS = {'url':url}
    
    r = requests.get(url = URL, params = PARAMS)
    
    data = r.json()

    result = eval(data['call'])

    general_prob_result = result['result']['general_probability'] 
    algorithem_result = result['result']['algorithms']
    
    binary_result = None
    agree = None
    disagree = None
    discuss = None
    unrelated = None
    
    for i in algorithem_result:
        if i['name'] == 'binary-classification':
            binary_result = eval(i['return'])['binary_classification_result']
        elif i['name'] == 'uclnlp':
            result = eval(i['return'])
            agree = result['agree']
            disagree = result['disagree']
            discuss = result['discuss']
            unrelated = result['unrelated']
        elif i['name'] == 'news-score':
            pass
        else:
            pass
    
    return general_prob_result,binary_result,agree,disagree,discuss,unrelated

true_data = pd.read_csv('data/true_links.csv')
false_data = pd.read_csv('data/true_links.csv')

true_data.columns = ['links']
false_data.columns = ['links']

pos = true_data.links.values.tolist()
neg = false_data.links.values.tolist()

data = pd.concat([pd.DataFrame(pos+neg),pd.DataFrame(np.ones(len(pos)).tolist()+np.zeros(len(neg)).tolist())], axis=1) 
data.columns = ['links','class']

# print(pos[0].strip())
data[['general prob','binary','agree','disagree','disuss','unrelated']] = data.links.apply(lambda x: pd.Series(call_result(x)))

# general,binary,uclnp = call_result(pos[0].strip())
data.to_csv('output.csv')