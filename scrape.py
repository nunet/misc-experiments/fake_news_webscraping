from selenium import webdriver
from dateutil import parser
import csv



  
driver = webdriver.Firefox()
  
# link = "https://www.snopes.com/fact-check/rating/true/"
# file_name = 'true_links.csv'

link = "https://www.snopes.com/fact-check/rating/false/"
file_name = 'false_links.csv'

prev_links = []
MAX_PAGES = 2

with open('data/'+file_name, 'w', newline='') as out_f: # Python 3
    w = csv.writer(out_f, delimiter=' ')   
    for i in range(MAX_PAGES):
        # try:
        if link not in prev_links:
            driver.get(link)   
            prev_links.append(link)
        else:
            continue


        links = driver.find_element_by_class_name("col-12.col-lg-8.mb-3")

        element = links.find_elements_by_class_name("stretched-link")


        for i in element:
            w.writerow([str(i.get_attribute('href'))])


        link = driver.find_element_by_class_name('page-link.font-weight-bold').get_attribute('href')

        print('new_link',link)

        # except:
        #     print("Error occured")
